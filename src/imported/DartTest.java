package imported;

/**
 * Classe de départ pour les tests de K-moyennes.
 * @author Pascal Dally-Bélanger
 *
 */
public class DartTest {

	/**
	 * Point d'entrée de la classe.
	 * @param args arguments de démarrage
	 */
	public static void main(String[] args) {
		new DartFrame().setVisible(true);
	}
}
