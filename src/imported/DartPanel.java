package imported;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;

import javax.swing.JPanel;

import main.Main;
import main.Main.Line;
import main.Main.Point;

public class DartPanel extends JPanel {

	private static final long serialVersionUID = -7072259667424377034L;


	private boolean isInit = false;
	private boolean animation = false;
	int dartCount = 50;

	private Point[] darts;
	ArrayList<Point> perimeter;
	ArrayList<Point> points;
	Queue<Line> queue = new ArrayDeque<>();
	private Point[] active = new Point[2];

	public DartPanel() {
		
	}

	private void init() {
		darts = new Point[dartCount];
		for(int i = 0; i < darts.length; i++)
			darts[i] = Point.generateRandom().mul(500).minus(new Point(-100, -100));
		
		perimeter = new ArrayList<>();
		points = new ArrayList<>(Arrays.asList(darts));
		
		Line midLine;
		{
			int xMinIndex = 0, xMaxIndex = 0;
			double xMin = darts[xMinIndex].x, xMax = darts[xMaxIndex].x;
			for(int i = 1; i < darts.length; i++) {
				if(darts[i].x < xMin) {
					xMin = darts[i].x;
					xMinIndex = i;
				} else if(darts[i].x > xMax) {
					xMax = darts[i].x;
					xMaxIndex = i;
				}
			}
			
			midLine = new Line(darts[xMinIndex], darts[xMaxIndex]);
		}
		
		queue = new ArrayDeque<>();
		queue.add(midLine);
		perimeter.add(midLine.p1);
		perimeter.add(midLine.p2);
		queue.add(midLine.swap());
		active[0] = midLine.p1;
		active[1] = midLine.p2;
		
		isInit = true;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		if (!isInit)
			init();

		g2d.setColor(Color.black);
		for (Point element : darts)
			g2d.draw(new Ellipse2D.Double(element.x - 2, element.y - 2, 4, 4));
		
		g2d.setColor(Color.blue);
		for (Point element : points)
			g2d.draw(new Ellipse2D.Double(element.x - 2, element.y - 2, 4, 4));
		

		g2d.setColor(Color.red);
		for (Point element : perimeter)
			g2d.draw(new Ellipse2D.Double(element.x - 2, element.y - 2, 4, 4));
		
		for(int i = 1; i < perimeter.size(); i++)
			g2d.drawLine((int) perimeter.get(i - 1).x, (int) perimeter.get(i - 1).y, (int) perimeter.get(i).x, (int) perimeter.get(i).y);
		g2d.drawLine((int) perimeter.get(perimeter.size() - 1).x, (int) perimeter.get(perimeter.size() - 1).y, (int) perimeter.get(0).x, (int) perimeter.get(0).y);
		
		g2d.setColor(Color.green);
		for (Point element : active)
			g2d.draw(new Ellipse2D.Double(element.x - 2, element.y - 2, 4, 4));
		
	}

	public void prochain() {
		Line line = queue.poll();
		Point p = Main.findMidpoint(line, points);
		active[0] = line.p1;
		active[1] = line.p2;
		System.out.printf("Found midpoint: %s between %s and %s\tStack: %s%n", p, line.p1, line.p2, queue.size());
		if(p != null) {
			perimeter.add(p);
			queue.add(new Line(p, line.p2));
			queue.add(new Line(line.p1, p));
		}
		
		Point middle = new Point(0, 0);
		for(int i = 0; i < perimeter.size(); i++)
			middle = middle.add(perimeter.get(i));
		middle = middle.mul(1d/perimeter.size());
		Point middle2 = middle;
		perimeter.sort((a, b) -> (int) Math.signum((a.minus(middle2).angle() + 2 * Math.PI % (2 * Math.PI)) - (b.minus(middle2).angle() + 2 * Math.PI % (2 * Math.PI))));
		if(queue.isEmpty())
			init();
		repaint();
	}

	public void reinitialiser() {
		init();
		repaint();
	}
	
	public void animer() {
		if (!animation)
			new Thread(() -> {
				animation = true;
				while (animation) {
					prochain();
					try {
						Thread.sleep(100L);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
	}

	public void arreter() {
		animation = false;
	}
}
