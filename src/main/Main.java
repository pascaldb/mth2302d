package main;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.TreeMap;
import java.util.function.BiConsumer;

public class Main {

	private static final int SAMPLE_SIZE = 100;
	private static final int DART_COUNT = 50;
	
	public static void main(String[] args) {
		TreeMap<Integer, Integer> polygonPerimeterUnified = new TreeMap<>();
		int[] polygonPerimeters = new int[SAMPLE_SIZE];
		int[] otherDiscrete = new int[SAMPLE_SIZE];
		double[] polygonAreas = new double[SAMPLE_SIZE];
		double[] polygonPerimetersLength = new double[SAMPLE_SIZE];
		double[] avgDistances = new double[SAMPLE_SIZE];
		double[] minDistances = new double[SAMPLE_SIZE];
		double[] maxDistances = new double[SAMPLE_SIZE];
		
		for(int i = 0; i < SAMPLE_SIZE; i++) {
			Point[] darts = generateDarts();
			Point[] perimeter = getPerimeter(darts);
			
			polygonPerimeterUnified.put(perimeter.length, polygonPerimeterUnified.getOrDefault(perimeter.length, 0) + 1);
			polygonPerimeters[i] = perimeter.length;
			
			for(int j = 1; j < perimeter.length; j++)
				polygonPerimetersLength[i] += perimeter[j - 1].distance(perimeter[j]);
			polygonPerimetersLength[i] += perimeter[perimeter.length - 1].distance(perimeter[0]);
			
			for(int j = 1; j < perimeter.length; j++)
				polygonAreas[i] += perimeter[j - 1].cross(perimeter[j]);
			polygonAreas[i] /= 2;
			
			Container<Double> average = new Container<>();
			average.t = 0d;
			
			Container<Double> min = new Container<>();
			min.t = 100d;
			
			Container<Double> max = new Container<>();
			max.t = 0d;
			
			forEachPairwise((a, b) -> {
				double dist = a.distance(b);
				average.t += dist;
				if(dist < min.t)
					min.t = dist;
				if(dist > max.t)
					max.t = dist;
			}, darts);
			
			avgDistances[i] = average.t / DART_COUNT / (DART_COUNT - 1) * 2;
			minDistances[i] = min.t;
			maxDistances[i] = max.t;
			
			List<Point> dartsList = new ArrayList<>(Arrays.asList(darts));
			int j;
			for(j = 0; dartsList.size() > 3; j++)
				dartsList.removeAll(Arrays.asList(getPerimeter(dartsList.toArray(new Point[0]))));
			otherDiscrete[i] = j;
			if(dartsList.size() == 0)
				otherDiscrete[i]--;
		}
		
		for(int i = 0; i < SAMPLE_SIZE; i++) {
			System.out.printf("%s\t%s\t%s\t%s\t%s\t%s\t%s%n", polygonPerimetersLength[i], otherDiscrete[i], polygonPerimeters[i], polygonAreas[i], avgDistances[i], minDistances[i], maxDistances[i]);
		}
		
	}
	
	public static Point[] generateDarts() {
		Point[] darts = new Point[DART_COUNT];
		for(int i = 0; i < darts.length; i++)
			darts[i] = Point.generateRandom();
		return darts;
	}
	
	public static class Line {
		public final Point p1, p2;
		
		public Line(Point p1, Point p2) {
			this.p1 = p1;
			this.p2 = p2;
		}
			
		public Line swap() {
			return new Line(p2, p1);
		}
	}
	
	public static Point[] getPerimeter(Point[] points2) {
		ArrayList<Point> perimeter = new ArrayList<>();
		ArrayList<Point> points = new ArrayList<>(Arrays.asList(points2));
		
		
		Line midLine;
		{
			int xMinIndex = 0, xMaxIndex = 0;
			double xMin = points2[xMinIndex].x, xMax = points2[xMaxIndex].x;
			for(int i = 1; i < points2.length; i++) {
				if(points2[i].x < xMin) {
					xMin = points2[i].x;
					xMinIndex = i;
				} else if(points2[i].x > xMax) {
					xMax = points2[i].x;
					xMaxIndex = i;
				}
			}
			
			midLine = new Line(points2[xMinIndex], points2[xMaxIndex]);
		}
		
		Queue<Line> queue = new ArrayDeque<>();
		queue.add(midLine);
		queue.add(midLine.swap());
		perimeter.add(midLine.p1);
		perimeter.add(midLine.p2);
		
		while(!queue.isEmpty()) {
			Line line = queue.poll();
			Point p = findMidpoint(line, points);
			if(p != null) {
				perimeter.add(p);
				queue.add(new Line(p, line.p2));
				queue.add(new Line(line.p1, p));
			}
		}
		
		Point middle = new Point(0, 0);
		for(int i = 0; i < perimeter.size(); i++)
			middle = middle.add(perimeter.get(i));
		middle = middle.mul(1d/perimeter.size());
		Point middle2 = middle;
		perimeter.sort((a, b) -> (int) Math.signum((a.minus(middle2).angle() + 2 * Math.PI % (2 * Math.PI)) - (b.minus(middle2).angle() + 2 * Math.PI % (2 * Math.PI))));
		
		
		return perimeter.toArray(new Point[0]);
	}
	
	
	public static Point findMidpoint(Line line, ArrayList<Point> points) {
		double max = Double.NEGATIVE_INFINITY;
		int maxIndex = -1;
		ArrayList<Integer> removalCandidates = new ArrayList<>();
		Point dp2 = line.p2.minus(line.p1).rot90();
		for(int i = 0; i < points.size(); i++) {
			if(points.get(i).equals(line.p1) || points.get(i).equals(line.p2))
				continue;
			double score = dp2.dot(points.get(i).minus(line.p1));
			if(score > max && score > 0) {
				maxIndex = i;
				max = score;
			}
			if(score > 0)
				removalCandidates.add(i);
		}
		if(maxIndex == -1)
			return null;
		Point p = points.get(maxIndex);
		Point dp1 = line.p1.minus(p).rot90(), dpm = p.minus(line.p2).rot90();
		
		for(int i = removalCandidates.size() - 1; i >= 0; i--) {
			int index = removalCandidates.get(i);
			if(dpm.dot(points.get(index).minus(line.p2)) > 0 && dp1.dot(points.get(index).minus(p)) > 0)
				points.remove(index);
		}
		return p;
	}
	
	public static <T> void forEachPairwise(BiConsumer<T, T> consumer, T[] array) {
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < i; j++)
				consumer.accept(array[j], array[i]);
	}
	
	private static class Container<T> {
		private T t;
	}
	
	public static class Point {
		public final double x, y;
		
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		
		public static Point generateRandom() {
			return new Point(Math.random(), Math.random());
		}
		
		public double distance(Point other) {
			return minus(other).length();
		}
		
		public Point minus(Point other) {
			return new Point(x - other.x, y - other.y);
		}
		
		public double dot(Point other) {
			return x * other.x + y * other.y;
		}
		
		public double cross(Point other) {
			return x * other.y - y * other.x;
		}
		
		public Point mul(double c) {
			return new Point(c * x, c * y);
		}
		
		public Point project(Point other) {
			return other.mul(dot(other)).mul(1/other.dot(other));
		}
		
		public Point rot90() {
			return new Point(-y, x);
		}
		
		public double length() {
			return Math.sqrt(x * x + y * y);
		}
		
		
		@Override
		public String toString() {
			return String.format("(%s, %s)", x, y);
		}

		public Point add(Point other) {
			return new Point(x + other.x, y + other.y);
		}

		public double angle() {
			return Math.atan2(y, x);
		}
	}
}
